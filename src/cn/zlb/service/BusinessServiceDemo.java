package cn.zlb.service;

import org.springframework.stereotype.Service;

import cn.zlb.dao.BusinessDaoDemo;
import cn.zlb.entity.TBusiness;
@Service
public class BusinessServiceDemo {
	private BusinessDaoDemo businessDao;
	/**
	 * 1.0 business 注册 
	 */
	
	public boolean regist(TBusiness tBusiness){
		
		return businessDao.regist(tBusiness);
	}
}

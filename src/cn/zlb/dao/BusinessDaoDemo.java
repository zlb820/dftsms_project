package cn.zlb.dao;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.crypto.Data;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.junit.Test;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.zlb.dto.BusinessDto;
import cn.zlb.entity.TBusiness;
import cn.zlb.entity.TStore;
import cn.zlb.tools.CommonUtils;
@Repository("businessDaoDemo") 
public class BusinessDaoDemo {
	@Resource
	private SessionFactory sessionFactory;
	/**
	 * 1.0添加一个对象demo
	 */
	public void addBusiness(){
		System.out.println("insert business");
		TBusiness business=new TBusiness();
		business.setPkBusId(CommonUtils.uuid());
		business.setBusName("zlb");
		business.setBusPass("zlb");
		business.setBusPhone("18292489328");
		business.setBusIdentity("zlb");
		business.setBusTime(new Timestamp(new Date().getTime()));
		business.setBusStatus(true);
		
		//TStore
		TStore store=new TStore();
		store.setStoName("zlbdp");
		store.setStoStatus(true);
		store.setStoScore("5");
		
		business.setTStore(store);
		store.setTBusiness(business);
		 
		Session session=sessionFactory.openSession();
		session.beginTransaction();
		session.save(business);
		session.getTransaction().commit();
		session.close();
	}
	/**
	 * 2.0查询 demo 
	 * @return
	 */
	
 public List  find(){

	 //query
	System.out.println("query 查询");
	 Session session=getSession();
			 
	 
	Query query= session.createQuery("from TBusiness");
	
	List<TBusiness>List=(List<TBusiness>)query.list();
	for (TBusiness tBusiness : List) {
		System.out.println(tBusiness.getBusName()+"  &&  store="+tBusiness.getTStore().getStoName());
	}
	 
	return  List;
	
	 
 }
 	/**
 	 * 3.0查找一个对象 根据 占位符条件查询
 	 * @return
 	*/
 public Object findObject(){
	 String hql="from TBusiness bus where bus.busName=? and bus.busPass=?";
	 //String hql="from TBusiness bus where bus.busName=:name and bus.busPass=:pass";
	 Query query= getSession().createQuery(hql);
	/*  query.setParameter("name","zlb");
	  query.setParameter("pass", "zlb");*/
	 query.setParameter(0, "zlb");
	 query.setParameter(1, "zlb");
	 return query.uniqueResult();
 }
 	/**
 	 * 4.0sql  'or' 查找
  	 * @param tBusiness
 	 * @return
 	 */
 public Object findObjectByCriteria(TBusiness business){
	 
	 Criteria cri= getSession().createCriteria(TBusiness.class).add(Restrictions.disjunction()
			 .add(Restrictions.eq("busName",business.getBusName()))
			 .add(Restrictions.eq("busPass", business.getBusPass()))
			 .add(Restrictions.eq("busEmail", business.getBusEmail())))
			 .add(Restrictions.eq("busPhone", business.getBusPhone())
			 );
	 return  cri.uniqueResult();
 }
 	/**
 	 * 5.0 根据对象setProperties(Object bean)查询
 	 * 
 	 * Bind the values of the given Map for each named parameters of the query, 
 	 * matching key names with parameter names and mapping value types to Hibernate types using heuristics.
 
		使用启发式方法，将查询的每个命名参数绑定给给定的Map的值，将关键字名称与参数名称和映射值类型绑定到Hibernate类型。
 	 * @return
 	 */
 
 public Object findObjectByProperties(){
	 TBusiness business=new TBusiness();
	 business.setBusName("zl");
	 Query query=getSession().createQuery("from TBusiness as bus  where 1=1 and "
	 		+ "bus.busName like  '%"+business.getBusName()+"%' ");
	 query.setProperties(business);
	 return query.list();
	 
 }
 
 public List<TBusiness> findObjectByPropertiesTime(){
	 BusinessDto business=new BusinessDto();
	 business.setBusTime(Timestamp.valueOf("2017-04-05 11:11:11"));
	 business.setEndTime(Timestamp.valueOf("2017-04-10 11:11:11"));
	 Query query=getSession().createQuery("from TBusiness as bus  where 1=1 and "
	 		+ "bus.busTime between :busTime and :endTime");
	
	 query.setDate("busTime", business.getBusTime());
	 query.setDate("endTime", business.getEndTime());
	 query.setFirstResult(0);
		query.setMaxResults(10);
	 return query.list();
	 
 }
 
 //demo
public boolean regist(TBusiness tBusiness) {
	System.out.println("tBusiness  success !!!");
	return true;
}
public Session getSession(){
	return sessionFactory.getCurrentSession();
	
}
}

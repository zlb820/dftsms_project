package cn.zlb.springtest.store;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;

import cn.zlb.dao.StoreDaoDemo;
import cn.zlb.dto.StoreDto;
import cn.zlb.entity.TStore;
import cn.zlb.service.StoreService;
import cn.zlb.springtest.BaseTest;
import cn.zlb.tools.Pager;

public class StoreTest  extends BaseTest{
	@Test
	public void print(){
		
		System.out.println("spring test");
	}
	@Resource
	private StoreService storeService;
	/**
	 * 1.0 按照dto查询
	 */
	@Test
	public void findStore(){
		StoreDto dto=new StoreDto();
		dto.setStoName("zlb");
		TStore store=storeService.findStore(dto);
		System.out.println("findStore =" +store);
		
	}
	/**
	 * 2.0 按照name查询
	 */
	@Test
	public void findByName(){
		StoreDto dto=new StoreDto();
		dto.setStoName("zlbdp");
		TStore store=storeService.findStoreByName(dto);
		System.out.println("findStore =" +store);
		
	}
	/**
	 * 3.0按照 店铺名 模糊查询
	 */
	@Test
	public void serachByName(){
		StoreDto dto=new StoreDto();
		dto.setStoName("zlb");
		Pager<TStore> storeList=storeService.serachStoreByName(dto,new Pager<TStore>());
		for (TStore tStore : storeList.getResultList()) {
			System.out.println("store name = " +tStore.getStoName());
		}
	}
	/**
	 * 4.0按照评分 区间查询
	 */
	@Test
	public void serachByScore(){
		StoreDto dto=new StoreDto();
		dto.setStoScore("4");
		dto.setStoMaxScore("10");
		Pager<TStore> storeList=storeService.serachStoreByScore(dto, new Pager<TStore>());
		for (TStore tStore : storeList.getResultList()) {
			System.out.println("store name = " +tStore.getStoName()+"   "+"score ="+tStore.getStoScore());
		}
		
		
	}
	
	//--------------demo -----------------
	// 4.0 demo
	@Resource
	private StoreDaoDemo storeDaoDemo;
	@Test
	public void serachByScoreDemo(){
		StoreDto dto=new StoreDto();
		dto.setStoScore("4");
		List<TStore >storeList=storeDaoDemo.serachStoreByScore(dto);
		for (TStore tStore : storeList ) {
			System.out.println("store name = " +tStore.getStoName()+"   "+"score ="+tStore.getStoScore());
		}
		
	}
}












package cn.zlb.springtest.comment;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import cn.zlb.dao.CommentDao;
import cn.zlb.dto.CommentDto;
import cn.zlb.entity.TComment;
import cn.zlb.entity.TCustomer;
import cn.zlb.entity.TStore;
import cn.zlb.service.CommentService;
import cn.zlb.service.StoreService;
import cn.zlb.springtest.BaseTest;
import cn.zlb.tools.CommonUtils;
import cn.zlb.tools.Pager;

public class CommentTest extends BaseTest {
	@Resource
	private CommentService commentService;
	/**
	 * 1.0添加评论
	 */
	@Test
	@Rollback(false)
	public void addComment(){
		TComment comment=new TComment();
		 
		TStore store=new TStore();
		store.setFkStoId("B6EF0F47A4F04CCB8434FBE77CA145BA");
		
		TCustomer cus=new TCustomer();
		cus.setPkCusId("1");
 
		
		
		comment.setPkComId(CommonUtils.uuid());
	 
		comment.setTStore(store);
		comment.setComLevel(2);
		comment.setComContent("老板娘很漂亮 喜欢");
		
		comment.setTCustomer(cus);
		Serializable id=commentService.add(comment);
		System.out.println("add commnet = " + comment.getPkComId());
		
		
		
	}
	/**
	 * 2.0 findComment
	 * @return
	 */
	@Test
	public void findComment(){
		TStore store=new TStore();
		store.setFkStoId("2D701DCB895A4CD3A41B262E34B3A690");
		
		CommentDto comDto=new CommentDto();
		comDto.setTStore(store);
		Pager<TComment> pager=commentService.findComment(comDto, new Pager<TComment>());
		
		for (TComment com : pager.getResultList()) {
			System.out.println("comment id = "+com.getPkComId() + " and "+"content = " +com.getComContent()  );
		}
	}
	
	/**
	 * 3.0 searchComment
	 * @return
	 */
	@Test
	public void serachComment(){
	 
		
		CommentDto comDto=new CommentDto();
		comDto.setComContent("喜欢");
		Pager<TComment> pager=commentService.searchComment(comDto, new Pager<TComment>());
		
		for (TComment com : pager.getResultList()) {
			System.out.println("comment id = "+com.getPkComId() + " and "+"content = " +com.getComContent()  );
		}
	}
	
	/**
	 * 4.0 评分查找
	 * @return
	 */
	@Test
	 
	public void findCommentLevel(){
		String stoId="120BB352E8FD410CB10E78771E7A6A56";
		int i=commentService.findStoreLevel(stoId);
		System.out.println("店铺星级 =  " +i);
		
	}
	
	@Resource
	private StoreService storeService;
	@Test
	public void stoList() {
		List<TStore> stoList=storeService.findStoreRecent();
		for (TStore tStore : stoList) {
			System.out.println("storeid = "+tStore.getFkStoId()+ "  " +"店铺评分 ="+commentService.findStoreLevel(tStore.getFkStoId()));
		}	
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	public StoreService getStoreService() {
		return storeService;
	}
	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}
	//setter
	public CommentService getCommentService() {
		return commentService;
	}
	
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	
}

package cn.zlb.springtest.business;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import cn.zlb.dao.BusinessDaoDemo;
import cn.zlb.dto.BusinessDto;
import cn.zlb.dto.StoreDto;
import cn.zlb.dto.android.business.BusFailedDto;
import cn.zlb.dto.android.business.BusinessDtoAnd;
import cn.zlb.entity.TBusiness;
import cn.zlb.entity.TPictures;
import cn.zlb.entity.TStore;
import cn.zlb.service.BusinessService;
import cn.zlb.service.CollectionService;
import cn.zlb.service.CommentService;
import cn.zlb.service.StoreService;
import cn.zlb.springtest.BaseTest;
import cn.zlb.tools.CommonUtils;
import cn.zlb.tools.Pager;
 
public class BusinessTest extends BaseTest {
	@Resource
	private BusinessDaoDemo businessDaoDemo;
	@Test
	public void TEST(){
		System.out.println("junit text");
		System.out.println(CommonUtils.uuid()+"and"+CommonUtils.uuid().length());
	}
	//------------------------------------------代码测试部分------------------------------	
	/**
	 * 1.0 添加商户 测试
	 */
	@Resource
	private BusinessService businessService;
	@Test
	//@Rollback(true)
	@Rollback(false)
	public void addBusiness(){
		TBusiness business=new TBusiness();
		business.setPkBusId(CommonUtils.uuid());
		business.setBusName("zlb03");
		business.setBusPass("zlb03");
		business.setBusPhone("18292489328");
		business.setBusIdentity("62254199804571318");
		business.setBusTime(new Timestamp(new Date().getTime()));
		business.setBusStatus(true);
		
		//TStore
		TStore store=new TStore();
		store.setStoName("zlb03dp");
		store.setStoStatus(true);
		store.setStoScore("5");
		
		 
		business.setTStore(store);
		store.setTBusiness(business);
		
		Serializable id=businessService.add(business);
		System.out.println("businessTest save business id = " +id);
		
	}
	
	/***
	 * 2.0 c测试登录
	 */
	@Test
	public void findObject(){
		TBusiness bus=new TBusiness();
		bus.setBusName("zlb");
		bus.setBusPass("zlb");
		TBusiness business=businessService.login(bus);
		System.out.println("登录用户= " +business) ;
	}
	
	/**
	 * 3.0 测试 properties条件查询
	 */
	@Test
	public void findByDto(){
		BusinessDto busDto=new BusinessDto();
		busDto.setBusTime(Timestamp.valueOf("2017-04-06 00:00:00"));
		//busDto.setEndTime(Timestamp.valueOf("2017-04-10 00:00:00"));
		Pager pager=new Pager();
		
		Pager<TBusiness> resultList =businessService.serachBusiness(busDto, pager);
		for (TBusiness tBusiness : resultList.getResultList()) {
			System.out.println("listByDto --- name= "+tBusiness.getBusName() + " date = " + tBusiness.getBusTime());
		}
	}
	/**
	 * 4.0 测试 商户 查询
	 */
	@Test
	public void findBusinessByName(){
		
		BusinessDto dto=new BusinessDto();
		dto.setBusName("zlb");
		// 根据商户名查询
		TBusiness business=businessService.findBusinessByName(dto);
		
		System.out.println("findbusiness name= "+business.getBusName());
	}
	/**
	 * 5.0模糊查询
	 */
	@Test
	public void searchBusiness(){
		
		BusinessDto dto=new BusinessDto();
		dto.setBusTime(Timestamp.valueOf("2017-04-05 11:11:11"));
		dto.setEndTime(Timestamp.valueOf("2017-04-10 11:11:11"));
		// 根据商户名查询
		Pager<TBusiness> busList=businessService.serachBusiness(dto,new Pager<TBusiness>());
		System.out.println("list size= " +busList.getResultList().size());
		for (TBusiness tBusiness : busList.getResultList()) {
			
			System.out.println("findbusiness name= "+tBusiness.getBusName());
		}
	}
	
	
	/**
	 * 5-1 查询用户信息 and
	 * @return
	 */
	
	     @Resource 
	     private StoreService storeService;
	     @Resource
	     private CommentService commentService;
	     //商户id
		 private String busId="120BB352E8FD410CB10E78771E7A6A56";
		 private BusinessDtoAnd busInfo;
		 //失败
		 private BusFailedDto busFailed;
		 Gson gson=new Gson();
		 @Test
	public void businessFindById(){
		BusinessDto dto=new BusinessDto();
		dto.setPkBusId(busId);
		TBusiness busResult=businessService.findBusiness(dto);
		
		//1-2查找 商家店铺信息
		StoreDto stoDto=new StoreDto();
		stoDto.setFkStoId(busId);
		TStore store =storeService.findStore(stoDto);
		if (busResult==null) {
			busFailed=new BusFailedDto();
			busFailed.setBusId(busResult.getPkBusId());
			System.out.println(gson.toJson(busFailed));
		//	return "busFailedResult";
		}else{
			busInfo=new BusinessDtoAnd();
			busInfo.setBusId(busResult.getPkBusId()); 
			busInfo.setBusEmail(busResult.getBusEmail());
			busInfo.setBusName(busResult.getBusName());
			busInfo.setBusPhone(busResult.getBusPhone());
			if (!busResult.getBusStatus()) {
				busInfo.setBusStatus("0");
			}
			busInfo.setBusStatus("1");
			busInfo.setBusTime(busResult.getBusTime()+"");
			busInfo.setBusPic(busResult.getTPictures().getPicUrl());
			
			
			//设置店铺信息
			busInfo.setSellCount(store.getTGood().getGooSales());
			//店铺销量
			busInfo.setBusAddress(store.getTAddress().getAddConcrete());
			//店铺星级
			busInfo.setBusLevel(commentService.findStoreLevel(store.getFkStoId())+"");
			//店铺 是否被当前用户收藏
			busInfo.setBusStar("0");
			System.out.println(gson.toJson(busInfo));
			//return "busResult";
		}
	}
		 
    /**
     * 6.0 查找用户是否收藏
     * @return
     */
		 @Resource
		 private CollectionService collectionService;
		 @Test
		 public void findCollection(){
			 String cusId="1";
			 String stoId="120BB352E8FD410CB10E78771E7A6A56";
			 boolean boo=collectionService.findCollection(cusId, stoId);
			 System.out.println("是否收藏  = "+boo);
		 }
		 
	  
	public CollectionService getCollectionService() {
			return collectionService;
		}

		public void setCollectionService(CollectionService collectionService) {
			this.collectionService = collectionService;
		}

	public StoreService getStoreService() {
			return storeService;
		}

		public void setStoreService(StoreService storeService) {
			this.storeService = storeService;
		}

		public CommentService getCommentService() {
			return commentService;
		}

		public void setCommentService(CommentService commentService) {
			this.commentService = commentService;
		}

	public BusinessDaoDemo getBusinessDaoDemo() {
			return businessDaoDemo;
		}

		public void setBusinessDaoDemo(BusinessDaoDemo businessDaoDemo) {
			this.businessDaoDemo = businessDaoDemo;
		}

		public String getBusId() {
			return busId;
		}

		public void setBusId(String busId) {
			this.busId = busId;
		}

		public BusinessDtoAnd getBusInfo() {
			return busInfo;
		}

		public void setBusInfo(BusinessDtoAnd busInfo) {
			this.busInfo = busInfo;
		}

		public BusFailedDto getBusFailed() {
			return busFailed;
		}

		public void setBusFailed(BusFailedDto busFailed) {
			this.busFailed = busFailed;
		}

		public Gson getGson() {
			return gson;
		}

		public void setGson(Gson gson) {
			this.gson = gson;
		}

		public BusinessService getBusinessService() {
			return businessService;
		}

	//------------------------------------------demo测试部分----------------------------------------
	/**
	 * 1.0占位符查询 demo 测试
	 */
	@Test
	@Transactional
	@Rollback(false)
	
	public void findObjectDemo(){
		TBusiness business=(TBusiness) businessDaoDemo.findObject();
		if (business==null) {
			System.out.println("find object= 没找到");
			
		}
		System.out.println("find object= "+business);
	}
	
	/**
	 * 2.0 properties查询
	 */
	@Test
	public void findObjectByPropertiesDemo(){
		List<TBusiness> busList=(List) businessDaoDemo.findObjectByProperties();
		System.out.println("properties 查询结果");
		for (TBusiness bus : busList) {
			System.out.println("busName= " +bus.getBusName());
		}
		
	}
	
	//demo  simpleDateFormat
	@Test
	public void dateFormat(){
		BusinessDto busDto=new BusinessDto();
		busDto.setBusTime(new Timestamp(System.currentTimeMillis()));
		System.out.println("date= "+busDto.getBusTime() );
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
		String dateStr=format.format(busDto.getBusTime());
		System.out.println("format date= "+dateStr);
		
	}
	
  	//demo add business
	@Test
	@Rollback(false)
	public void addBusinessDemo(){
		businessDaoDemo.addBusiness();
		
	}
	
	//find by criteria demo
	
	@Test
	public void findByCriteria(){
		TBusiness business=new TBusiness();
		business.setBusName("zlb");
		business.setBusPass("aliabab");
		business.setBusPhone("18292489328");
		TBusiness result=(TBusiness) businessDaoDemo.findObjectByCriteria(business);
		System.out.println("查询结果： "+result.getBusName()+" and "+result.getBusPass()+" and "+ result.getBusPhone()+" and "+result.getBusEmail());
	}
	//find by time
	@Test
	public void findByTime(){
		 
		List<TBusiness >result= businessDaoDemo.findObjectByPropertiesTime();
		System.out.println("list size = " +result.size());
		for (TBusiness tBusiness : result) {
			System.out.println("name = "+tBusiness.getBusName()+"time = "+ tBusiness.getBusTime());
		}
	}
	//find demo
	 @Test
	//@Transactional
	//@Rollback(false)
	public void find(){
		List<TBusiness>list= businessDaoDemo.find();
		System.out.println("test----------");
		for (TBusiness tBusiness : list) {
			System.out.println(tBusiness.getBusName()+"  &&  store="+tBusiness.getTStore().getStoName());
		}
	 
	}
	 
	 
	 
	 
	public void setBusinessService(BusinessService businessService) {
		this.businessService = businessService;
	}
	
	
	
}

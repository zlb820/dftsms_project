package cn.zlb.springtest.cheap;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;
import org.junit.Test;
import org.springframework.stereotype.Component;
import org.springframework.test.annotation.Rollback;

import cn.zlb.dao.BusinessDao;
import cn.zlb.entity.TCheap;
import cn.zlb.entity.TStore;
import cn.zlb.service.CheapService;
import cn.zlb.springtest.BaseTest;
import cn.zlb.tools.CheapDto;
import cn.zlb.tools.CommonUtils;

public class CheapTest extends BaseTest {
	@Resource
	private CheapService cheapService;
	/**
	 * 1.0 add 
	 */
	@Test
	@Rollback(false)
	public void addCheap(){
		TCheap cheap=new TCheap();
		cheap.setPkCheId(CommonUtils.uuid());
		cheap.setCheNam("满三十减十");
		
		TStore store=new TStore();
		store.setFkStoId("2D701DCB895A4CD3A41B262E34B3A690");
		cheap.setTStore(store);
		Serializable id=cheapService.add(cheap);
		System.out.println("add cheap = "+id);
		
	}
	/**
	 * 2.0 find
	 * @return
	 */
	@Test
	public void findCheap(){
		CheapDto dto=new CheapDto();
		dto.setCheNam("满十减三");
		List<TCheap> list=cheapService.findCheap(dto);
		for (TCheap tCheap : list) {
			System.out.println("cheap name = " +tCheap.getCheNam());
		}
		
	}
	/**
	 * 3.0 模糊查询
	 */
	@Test
	public void searchCheap(){
		CheapDto dto=new CheapDto();
		dto.setCheNam("满十");
		List<TCheap> list=cheapService.searchCheap(dto);
		for (TCheap tCheap : list) {
			System.out.println("cheap name = " +tCheap.getCheNam());
		}
		
	}
	//setter
	public CheapService getCheapService() {
		return cheapService;
	}
	public void setCheapService(CheapService cheapService) {
		this.cheapService = cheapService;
	}
	
	
	//-----------------test------------
	/**
	 * 自定义的bean 注解：1》application中需要配置bean所在package
	 * 				2》 @component注解 初始化bean
	 * 				3》@resource注解 装配bean
	 */
	@Resource
	private print print;
	
	 
	public print getPrint() {
		return print;
	}
	public void setPrint(print print) {
		this.print = print;
	}
	@Test
	public void springBeanTest(){
		print.print();
		
	}
	
	 
}









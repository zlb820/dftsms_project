package cn.zlb.springtest.category;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import cn.zlb.dao.CategoryDao;
import cn.zlb.dto.CategoryDto;
import cn.zlb.entity.TStore;
import cn.zlb.entity.TStoreCategory;
import cn.zlb.service.CategoryService;
import cn.zlb.springtest.BaseTest;
import cn.zlb.tools.CommonUtils;
import uk.ltd.getahead.dwr.util.Logger;

public class CategoryTest extends BaseTest {
	@Resource
	private CategoryService categoryService;
	static Logger logger=Logger.getLogger(CategoryTest.class);
	/**
	 * 1.0 添加目录
	 */
	@Test
	@Rollback(false)
	public void addCategory(){
		TStoreCategory cate=new TStoreCategory();
		cate.setPkCatId(CommonUtils.uuid());;
		cate.setCatName("小吃类");
		cate.setCatDesc(2);
		cate.setCatPid("0");
		
		
		TStore store=new TStore();
		store.setFkStoId("120BB352E8FD410CB10E78771E7A6A56");
		
		cate.setTStore(store);
		
		 
		Serializable id=categoryService.add(cate);
		logger.info("add category = "+id);
		
	}
	/**
	 * 1.1 修改 目录
	 */
	@Test
	@Rollback(false)
	public void updateCategory(){
		TStoreCategory cate=new TStoreCategory();
		cate.setPkCatId("1740D7031FFD46939200D3F967CB49F2");
		cate.setCatName("炒菜类");
		cate.setCatPid("0");
		cate.setCatDesc(2);
		cate.setCatPid("790B10DE333045258D5A8DF7BFEBC545");
		
		TStore store=new TStore();
		store.setFkStoId("2D701DCB895A4CD3A41B262E34B3A690");
		cate.setTStore(store);
		categoryService.saveOrUpdate(cate);
	}
	/**
	 * 2.0 查找目录
	 */
	@Test
	public void findCategory(){
	CategoryDto catDto=new CategoryDto();
	catDto.setCatName("凉菜");
	List<TStoreCategory> catList=categoryService.findCategory(catDto);
	System.out.println("category name = " +catList.get(0).getCatName());
}
	
	/**
	 * 3.0 查询所有一级目录
	 * @return
	 */
	@Test
	public void findAllPid(){
		List<TStoreCategory> listPid=categoryService.findAllPid("2D701DCB895A4CD3A41B262E34B3A690");
		for (TStoreCategory tStoreCategory : listPid) {
			System.out.println("一级目录 = " + tStoreCategory.getCatName());
		}
	}
	@Test
	public void findByPid(){
		List<TStoreCategory> listPid=categoryService.findCategoryByPid("790B10DE333045258D5A8DF7BFEBC545");
		for (TStoreCategory tStoreCategory : listPid) {
			System.out.println("二级目录 = " + tStoreCategory.getCatName());
		}
	}
	/**
	 * 3.1 查询所有一级目录
	 * @return
	 */
	@Test
	public void findAllCategory(){
		List<TStoreCategory> listPid=categoryService.findAllCategory("2D701DCB895A4CD3A41B262E34B3A690");
		for (TStoreCategory parent : listPid) {
			System.out.println("一级目录 = " + parent.getCatName());
			 List<TStoreCategory> child=parent.getListCategory();
			 for (TStoreCategory chi : child) {
				System.out.println("\t"+"二级目录= "+chi.getCatName());
			}
		}
	}
	
	
	
	//setter
	public CategoryService getCategoryService() {
		return categoryService;
	}

	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	
	
	
}

package cn.zlb.springtest.order;

import java.util.ArrayList;
import java.util.List;

 

import org.junit.Test;

import com.google.gson.Gson;

import cn.zlb.dto.android.order.BaseOrderDtoAnd;
import cn.zlb.dto.android.order.OrderDtoAndDetail;
import cn.zlb.dto.android.order.OrderNode;
import cn.zlb.dto.android.order.OrderWithStore;
import cn.zlb.dto.android.store.StoreDtoAndDetail;
import cn.zlb.dto.android.store.StoreNode;

public class OrderDtoTest  {
	/*private static BaseOrderDtoAnd<OrderWithStore>  baseOrderDto=new BaseOrderDtoAnd<OrderWithStore>();*/
	 //数组形式的 pageItems
	private static BaseOrderDtoAnd baseOrderDto=new BaseOrderDtoAnd();
	 @Test
	/*public static  BaseOrderDtoAnd<OrderWithStore>  OrderManager(){
		//order 
		OrderDtoAndDetail detail=new OrderDtoAndDetail();
		detail.setPicUrl("");
		detail.setOrderState("1");
		detail.setOrderTime("");
		detail.setPrice("20");
		detail.setOrderTitle("order01");
		//order02
		OrderDtoAndDetail detail2=new OrderDtoAndDetail();
		detail2.setPicUrl("");
		detail2.setOrderState("1");
		detail2.setOrderTime("");
		detail2.setPrice("20");
		detail2.setOrderTitle("order02");
		//orderList
		List<OrderDtoAndDetail> orderList=new ArrayList<OrderDtoAndDetail>();
		orderList.add(detail);
		orderList.add(detail2);
		//orderNode
		OrderNode<OrderDtoAndDetail> orderNode=new OrderNode<OrderDtoAndDetail>();
		orderNode.setOrders(orderList);
		
		
		//------------------------
		StoreDtoAndDetail detail3=new StoreDtoAndDetail();
		detail3.setAddress("");
		detail3.setImgUrl("");
		detail3.setSellCount("22");
		detail3.setShopTitle("zlbdp01");
		detail3.setSort("");
		detail3.setStartCount("");
		detail3.setTypeId("");
		
		StoreDtoAndDetail detail4=new StoreDtoAndDetail();
		detail4.setAddress("");
		detail4.setImgUrl("");
		detail4.setSellCount("22");
		detail4.setShopTitle("zlbdp02");
		detail4.setSort("");
		detail4.setStartCount("");
		detail4.setTypeId("");
		
		//storeList
		List<StoreDtoAndDetail> storeList=new ArrayList<StoreDtoAndDetail>();
		storeList.add(detail3);
		storeList.add(detail4);
		
		//storeNOde
		StoreNode<StoreDtoAndDetail> storeNode=new StoreNode<StoreDtoAndDetail>();
		storeNode.setList(storeList);
		
		//orderwithstore
		OrderWithStore orderWithStore=new OrderWithStore();
		orderWithStore.setStoreNode(storeNode);
		orderWithStore.setOrderNode(orderNode);
		
		baseOrderDto.setPageItems(orderWithStore);
		
		
		baseOrderDto.setPagestamp("2");
		
		Gson gson=new Gson();
		System.out.println(gson.toJson(baseOrderDto));
		 
		return baseOrderDto;
		
		
		
	}*/
	 public static  BaseOrderDtoAnd   OrderManager(){
			//order 
			OrderDtoAndDetail detail=new OrderDtoAndDetail();
			detail.setPicUrl("");
			detail.setOrderState("1");
			detail.setOrderTime("");
			detail.setPrice("20");
			detail.setOrderTitle("order01");
			//order02
			OrderDtoAndDetail detail2=new OrderDtoAndDetail();
			detail2.setPicUrl("");
			detail2.setOrderState("1");
			detail2.setOrderTime("");
			detail2.setPrice("20");
			detail2.setOrderTitle("order02");
			//orderList  node里的list
			List<OrderDtoAndDetail> orderList=new ArrayList<OrderDtoAndDetail>();
			orderList.add(detail);
			orderList.add(detail2);
			//orderNode
			OrderNode<OrderDtoAndDetail> orderNode=new OrderNode<OrderDtoAndDetail>();
			orderNode.setOrders(orderList);
			
			
			//------------------------
			StoreDtoAndDetail detail3=new StoreDtoAndDetail();
			detail3.setAddConcrete("");;
			detail3.setImgUrl("");
			detail3.setSaleValume("");
			detail3.setStoName("");
			detail3.setSort("");
			detail3.setScore("");
			detail3.setStyleId("");
			
			StoreDtoAndDetail detail4=new StoreDtoAndDetail();
			detail4.setAddConcrete("");;
			detail4.setImgUrl("");
			detail4.setSaleValume("22");
			detail4.setStoName("zlbdp02");
			detail4.setSort("");
			detail4.setScore("");
			detail4.setStyleId("");
			 
			
			//storeList
			List<StoreDtoAndDetail> storeList=new ArrayList<StoreDtoAndDetail>();
			storeList.add(detail3);
			storeList.add(detail4);
			
			//storeNOde
			StoreNode<StoreDtoAndDetail> storeNode=new StoreNode<StoreDtoAndDetail>();
			storeNode.setList(storeList);
			
			//orderwithstore
			OrderWithStore orderWithStore=new OrderWithStore();
			orderWithStore.setStoreNode(storeNode);
			orderWithStore.setOrderNode(orderNode);
			
			
			//pageItems 数组
			Object[] obj=new Object[2];
			obj[0]=storeNode;
			obj[1]=orderNode;
			baseOrderDto.setPageItems(obj);;
			
			
			baseOrderDto.setPagestamp("2");
			
			Gson gson=new Gson();
			System.out.println(gson.toJson(baseOrderDto));
			 
			return baseOrderDto;
			
			
			
		}
	 
	 public static void main(String[] args) {
	 OrderDtoTest test=new OrderDtoTest();
	 test.OrderManager();
	}
}

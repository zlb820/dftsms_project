package cn.zlb.springtest.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.hibernate.annotations.Source;
import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import cn.zlb.dto.OrderAddDto;
import cn.zlb.dto.OrderDto;
import cn.zlb.dto.TItemorderDto;
import cn.zlb.dto.android.NotFoundDto;
import cn.zlb.dto.android.order.BaseOrderDtoAnd;
import cn.zlb.dto.android.order.BaseOrderDtoAndAll;
import cn.zlb.dto.android.order.OrderDtoAndDetail;
import cn.zlb.dto.android.order.OrderNode;
import cn.zlb.dto.android.order.orderDetail.ItemOrderDetail;
import cn.zlb.dto.android.order.orderDetail.OrderDetailDetail;
import cn.zlb.dto.android.order.orderDetail.OrderDetailDtoAnd;
import cn.zlb.dto.android.store.StoreDtoAndDetail;
import cn.zlb.dto.android.store.StoreNode;
import cn.zlb.entity.TCustomer;
import cn.zlb.entity.TGoods;
import cn.zlb.entity.TItemorder;
import cn.zlb.entity.TOrder;
import cn.zlb.entity.TStore;
import cn.zlb.service.CommentService;
import cn.zlb.service.GoodsService;
import cn.zlb.service.ItemOrderService;
import cn.zlb.service.OrderService;
import cn.zlb.service.StoreService;
import cn.zlb.springtest.BaseTest;
import cn.zlb.tools.CommonUtils;
import uk.ltd.getahead.dwr.util.Logger;

public class OrderTest extends BaseTest {
	@Resource
	private OrderService orderService;
	@Resource
	private StoreService storeService;
	@Resource
	private CommentService commentService;
	static Logger logger=Logger.getLogger(OrderTest.class);
	private static Gson gson=new Gson();
	/**
	 * 1.0添加订单
	 */
	@Test
	@Rollback(false)
	public void addOrder(){
		TOrder order=new TOrder();
		order.setPkOrdId(CommonUtils.uuid());
		order.setOrdTime(new Timestamp(System.currentTimeMillis()));
		order.setOrdStatus((byte) 6);
		order.setOrdTotal("25");
		order.setOrdAddress("安徽省 阜阳市 亳州");
		 
		
		TCustomer customer=new TCustomer();
		customer.setPkCusId("1");
		
		TStore store=new TStore();
		store.setFkStoId("120BB352E8FD410CB10E78771E7A6A56");
		
		order.setTStore(store);
		order.setTCustomer(customer);
		
		Serializable id=orderService.add(order);
		logger.info("add order = "+ id);

		
	}
	/**
	 * 2.0修改订单总计
	 * @return
	 */
	@Test
	public void modifyOrderTotal(){
		OrderDto ordDto=new OrderDto();
		ordDto.setPkOrdId("506BAF29C9344061A80F746E6981B820");;
		ordDto.setOrdTotal("230");
		
		int i = orderService.modifyOrderTotal(ordDto);
		logger.info("i = "+i);
	}
	
	/**
	 * 3.0修改订单总计
	 * @return
	 */
	@Test
	public void modifyOrderStatus(){
		OrderDto ordDto=new OrderDto();
		ordDto.setPkOrdId("467A95C016774D6CA0A0FC2ABCA0DA64");;
		ordDto.setOrdStatus((byte) 4);
		
		int i = orderService.modifyOrderStatus(ordDto);
		logger.info("i = "+i);
	}
	
	/**
	 * 4.0 订单detail json
	 * @return
	 */
	@Test
	@Rollback(true)
	public void orderInfo(){
		  BaseOrderDtoAnd orderInfo=new BaseOrderDtoAnd();
		  /*	List<TOrder> ordList=orderService.findOrderByCusRecent("1");
		Object[] obj = orderInfoManage(ordList);
		orderInfo.setPagestamp("2");
		orderInfo.setPageItems(obj);*/
		
		//1.0 查找用户最近订单
				List<TOrder> ordList=orderService.findOrderByCusRecent("1");
				Object[] obj = new Object[2];
				obj[0]=orderInfoManage(ordList);
				
				//2.0查找最近店铺
				obj[1] = storeInfoManage();
				    
				   
				orderInfo.setPageItems(obj);
				orderInfo.setPagestamp("2");
		Gson gson=new Gson();
		System.out.println(gson.toJson(orderInfo));
	}
	 
	public StoreNode<StoreDtoAndDetail> storeInfoManage() {
		List<TStore> stoList=storeService.findStoreRecent();
		
		StoreDtoAndDetail stoDetail=null;
		List<StoreDtoAndDetail>  stoDetailList=new ArrayList<StoreDtoAndDetail>();
		StoreNode<StoreDtoAndDetail> stoNode=null;
		
		for (TStore store : stoList) {
			stoDetail=new StoreDtoAndDetail();
			stoDetail.setStoID(store.getFkStoId());;
			stoDetail.setStoName(store.getStoName());
			stoDetail.setImgUrl(store.getTPictures().getPicUrl());
			stoDetail.setSaleValume(store.getTGood().getGooSales());
			stoDetail.setAddConcrete(store.getTAddress().getAddConcrete());
			stoDetail.setScore(commentService.findStoreLevel(store.getFkStoId())+"");
			
			stoDetailList.add(stoDetail);
		}
		
		   stoNode=new  StoreNode<StoreDtoAndDetail>();
		   stoNode.setList(stoDetailList);
		   

		return stoNode;
	}
	private OrderNode<OrderDtoAndDetail> orderInfoManage(List<TOrder> ordList) {
		OrderDtoAndDetail ordDetail = null;
		List<OrderDtoAndDetail> ordDetailList=new ArrayList<OrderDtoAndDetail>();
		OrderNode<OrderDtoAndDetail> ordNode=null;
		for (TOrder order : ordList) {
			ordDetail=new OrderDtoAndDetail();
			ordDetail.setTypeId("31");
			ordDetail.setOrderTitle(order.getTStore().getStoName());
			ordDetail.setOrderState(order.getOrdStatus()+"");
			ordDetail.setOrderTime(order.getOrdTime().toString());
			ordDetail.setPrice(order.getOrdTotal());
			ordDetail.setPicUrl(order.getTStore().getTPictures().getPicUrl());
			
			ordDetailList.add(ordDetail);
		}
		ordNode=new OrderNode<OrderDtoAndDetail>();
		ordNode.setOrders(ordDetailList);
		 
		return ordNode;
	}
	
	
	//test store
	@Test
	public void stoList() {
		List<TStore> stoList=storeService.findStoreRecent();
		for (TStore tStore : stoList) {
			System.out.println("storeid = "+tStore.getFkStoId()+ "  " +"店铺评分 =");
		}	
		
	}
	
	/**
	 * 4.0 评分查找
	 * @return
	 */
	@Test
	 
	public void findCommentLevel(){
		String stoId="120BB352E8FD410CB10E78771E7A6A56";
		 commentService.findStoreLevel(stoId);
		System.out.println("店铺星级 =  " +commentService.findStoreLevel(stoId));
		
	}
	
	
	/**
	 * 5.0查看用户所有订单
	 * @return
	 */
	@Test
	public void orderAll(){
		  BaseOrderDtoAndAll<OrderDtoAndDetail> orderAll =new BaseOrderDtoAndAll<OrderDtoAndDetail>();
		//1.0 找到用户所有订单
		List<TOrder> ordList=orderService.findOrderByCus("1");
		
		//2.0 获取 orderDtoDetail 的集合 list
		List<OrderDtoAndDetail> orderDetailList=orderInfoManage(ordList).getOrders();
		
		//3.0   设置 pageitems
		orderAll.setPageItems(orderDetailList);
		 
		System.out.println(gson.toJson(orderAll));
	}
	
	/**
	 * 6.0根据订单状态查询
	 * @return
	 */
	@Test
	public void orderFindByStatus(){
		 BaseOrderDtoAndAll<OrderDtoAndDetail> orderAll =new BaseOrderDtoAndAll<OrderDtoAndDetail>();
		 //1.0 找到用户所有订单
		     List<TOrder> ordList=orderService.findOrderByStatus("8");
				
		 //2.0 获取 orderDtoDetail 的集合 list
			 List<OrderDtoAndDetail> orderDetailList=orderInfoManage(ordList).getOrders();
				
		 //3.0   设置 pageitems  orderAll 对象需要new
			 orderAll=new BaseOrderDtoAndAll<OrderDtoAndDetail>();
			 orderAll.setPageItems(orderDetailList);
				 
			 System.out.println(gson.toJson(orderAll));	
		 
	}
	
	
	/**
	 * 7.0 订单详情查看
	 * @return
	 */
	@Resource
	private  ItemOrderService itemOrderService;
	private String pkOrdId="506BAF29C9344061A80F746E6981B820";
	@Test
	public void orderFindById(){
		OrderDetailDtoAnd<OrderDetailDetail<ItemOrderDetail>> orderDetail=new OrderDetailDtoAnd<OrderDetailDetail<ItemOrderDetail>>();
		
		//1.0找到用户订单
		TOrder order=orderService.findOrderById(pkOrdId);
		//2.0找到order 所属的 itemOrder
		List<TItemorder> itemList=itemOrderService.findItemByOrderId(pkOrdId);
		//获取orderDD
		OrderDetailDetail<ItemOrderDetail> orderDD = orderDetailManage(order, itemList);
		//
		orderDetail.setPageItems(orderDD);
		
		//request.put("order", orderDetail);
		 System.out.println(gson.toJson(orderDetail));
		//return "orderResult";
	}
	private OrderDetailDetail<ItemOrderDetail> orderDetailManage(TOrder order, List<TItemorder> itemList) {
		//1.0-1  orderDetail 信息
		OrderDetailDetail<ItemOrderDetail> orderDD=new OrderDetailDetail<ItemOrderDetail>();
		orderDD.setStorId(order.getTStore().getFkStoId());
		orderDD.setShopImg(order.getTStore().getTPictures().getPicUrl());
		orderDD.setOrderState(order.getOrdStatus()+"");
		orderDD.setOrderNum(order.getPkOrdId());
		orderDD.setTime(order.getOrdTime()+"");
		orderDD.setAllPrice(order.getOrdTotal());
		
		//1.0-2 判断订单状态设置 payPrice （已经支付的金额）
		if ((order.getOrdStatus()+"").equals(1+"")) {
			orderDD.setPayPrice(0+"");
		}else{
			orderDD.setPayPrice(order.getOrdTotal());
		}
		
			//3.0遍历设置
			ItemOrderDetail itemOrder=null;
			List<ItemOrderDetail> itemOrderList=new ArrayList<ItemOrderDetail>();
			for (TItemorder item : itemList) {
				itemOrder=new ItemOrderDetail();
				itemOrder.setCount(item.getIteQuantity()+"");
				itemOrder.setPrice(item.getIteSubtotal());
				itemOrder.setGoodName(item.getTGoods().getGooName());
				
				//添加 到list
				itemOrderList.add(itemOrder);
			}

				orderDD.setList(itemOrderList);
				return orderDD;
		}
		/**
		 * 8.0 提交订单
		 * @return
		 */
	private String itemOrderJson="[\n" +
            "    {\n" +
            "        \"gooCurrprice\": \"26.0\",\n" +
            "        \"gooId\": \"A9263694F5C94874B00BA871091DDC54\",\n" +
            "        \"iteQuantity\": \"1\",\n" +
            "        \"iteSubtotal\": \"26.0\"\n" +
            "    }\n" +
            "]";
	@Resource
	private GoodsService goodsService;
	@Test
	@Rollback(false)
	public void orderAdd(){
		
		System.out.println("提交订单");
		//   解析itemOrderJson字符串获得订单项信息
		/*OrderAddDto<TItemorderDto> orderAddDto=gson.fromJson(itemOrderJson,new TypeToken<OrderAddDto<TItemorderDto>>(){}.getType());
		List<TItemorderDto> itemList=orderAddDto.getItemOrder();*/
		List<TItemorderDto> itemList=gson.fromJson(itemOrderJson, new TypeToken<List<TItemorderDto>>(){}.getType()); 
		//1-1 创建 order
		TCustomer customer=new TCustomer();
		//customer.setPkCusId(orderAddDto.getCusId());
		customer.setPkCusId("1");
		
		TOrder order=new TOrder();
		order.setPkOrdId(CommonUtils.uuid());
		order.setOrdStatus((byte) 1);
		order.setOrdTime(new Timestamp(System.currentTimeMillis()));
		order.setTCustomer(customer);
	
		
		
		//1-1-2 计算订单 总价
		BigDecimal total=new BigDecimal("0");
		for (TItemorderDto item  : itemList) {
			//获取总价格
			total=total.add(new BigDecimal(item.getIteSubtotal()+""));
		}
		order.setOrdTotal(total.toString());
		//1-1-3 添加 store属性
			 //获取店铺
			 TStore store=goodsService.findGoodsById(itemList.get(0).getGooId()).getTStore();
			 order.setTStore(store);
			 	
		//1-1-3 存储订单
		orderService.add(order);
		
		//------------------------------------------
		//1-2 添加提交的itemOrder
		
		/*//1-1 创建 order
		TCustomer customer=new TCustomer();
		customer.setPkCusId("2");
		 
		TOrder order=new TOrder();
		order.setPkOrdId(CommonUtils.uuid());
		order.setOrdStatus((byte) 1);
		order.setOrdTime(new Timestamp(System.currentTimeMillis()));
		order.setTCustomer(customer);
	
		//1-2 添加提交的itemOrder
			//1-1-1  解析itemOrderJson字符串获得订单项信息
		
		List<TItemorderDto> itemList=gson.fromJson(itemOrderJson, new TypeToken<List<TItemorderDto>>(){}.getType()); */
		System.out.println("List<TItemorderDto>:-------------");
		for (TItemorderDto item : itemList) {
			System.out.println("商品id :" +item.getGooId());
		}
		 	//存储订单项
		List<TItemorder> itemOrderList=new ArrayList<TItemorder>();
		//BigDecimal total=new BigDecimal("0");
		for (TItemorderDto item  : itemList) {
			TItemorder itemOrder=new TItemorder();
			//itemOrder添加基本属性
			itemOrder.setGooCurrprice(item.getGooCurrprice());
			itemOrder.setIteQuantity(item.getIteQuantity());
			itemOrder.setIteSubtotal(item.getIteSubtotal());
			itemOrder.setTOrder(order);
			//itemOrder 添加商品
			TGoods good=goodsService.findGoodsById(item.getGooId());
			itemOrder.setTGoods(good);
			//itemOrder  添加商品图片属性
			itemOrder.setGooImageL(good.getTPicturesByFkPictureSmall().getPicUrl());
			//添加 order
			itemOrder.setTOrder(order);
			//存储itemOrder 
			itemOrderService.add(itemOrder);
			//获得itemOrder集合
			itemOrderList.add(itemOrder);
			//获取总价格
			//total=total.add(new BigDecimal(item.getIteSubtotal()+""));
		}
		
		/*//1-3 添加 order属性
			//获取店铺
		 	TStore store=goodsService.findGoodsById(itemList.get(0).getGooId()).getTStore();
		 	order.setTStore(store);
		 	
		 	order.setOrdTotal(total.toString());*/
		 	Set itemOrderSet=new HashSet(itemOrderList);
		 	order.setTItemorders(itemOrderSet);
		 //1-4 保存订单
		
		//orderService.add(order);
		
		System.out.println("添加订单:-----------------------------");
		System.out.println("orderid = "+order.getPkOrdId());
		//request.put("order", order);
		
		//mes=NotFoundDto.SUCCESS();
		//return "mesResult";
	}
	
	
	
	
	
	
	
	public ItemOrderService getItemOrderService() {
		return itemOrderService;
	}
	public void setItemOrderService(ItemOrderService itemOrderService) {
		this.itemOrderService = itemOrderService;
	}
	public String getPkOrdId() {
		return pkOrdId;
	}
	public void setPkOrdId(String pkOrdId) {
		this.pkOrdId = pkOrdId;
	}
	public String getItemOrderJson() {
		return itemOrderJson;
	}
	public void setItemOrderJson(String itemOrderJson) {
		this.itemOrderJson = itemOrderJson;
	}
	public GoodsService getGoodsService() {
		return goodsService;
	}
	public void setGoodsService(GoodsService goodsService) {
		this.goodsService = goodsService;
	}
	//setter
	public OrderService getOrderService() {
		return orderService;
	}

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}
	public StoreService getStoreService() {
		return storeService;
	}
	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}
	public CommentService getCommentService() {
		return commentService;
	}
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}
    
	
	
}













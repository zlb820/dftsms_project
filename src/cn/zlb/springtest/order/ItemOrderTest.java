package cn.zlb.springtest.order;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import cn.zlb.dao.ItemOrderDao;
import cn.zlb.dto.TItemorderDto;
import cn.zlb.entity.TGoods;
import cn.zlb.entity.TItemorder;
import cn.zlb.entity.TOrder;
import cn.zlb.service.ItemOrderService;
import cn.zlb.springtest.BaseTest;
import cn.zlb.tools.CommonUtils;
import uk.ltd.getahead.dwr.util.Logger;

public class ItemOrderTest extends BaseTest {
	@Resource
	private ItemOrderDao itemOrderDao;
	@Resource
	private ItemOrderService itemOrderService;
	Logger logger=Logger.getLogger(ItemOrderTest.class);
	/**
	 * 1.0 添加订单项
	 */
	@Test
	@Rollback(false)
	public void addItemOrder(){
		TItemorder itemOrder=new TItemorder();
		itemOrder.setPkIteId(CommonUtils.uuid());
		itemOrder.setGooCurrprice("20");
		itemOrder.setIteQuantity(1);
		itemOrder.setIteSubtotal("20");
		
		TGoods goods=new TGoods();
		goods.setPkGooId("6ED5C50692EE4E33B522229A90AE5962");
		
		TOrder order =new TOrder();
		order.setPkOrdId("506BAF29C9344061A80F746E6981B820");
		
		itemOrder.setTOrder(order);
		itemOrder.setTGoods(goods);
		
		Serializable id=itemOrderDao.add(itemOrder);
		logger.info("itemOrder id = " + id);
		
		
	}
	
	/**
	 * 2.0根据  订单项 id 查询
	 * @return
	 */
	@Test
	public void findItemOrderById(){
		String itemId="449E99B13FF745B6B42353FE216AABB3";
		TItemorder item= itemOrderService.findItem(itemId);
		logger.info("itemorder   = "+item.toString());
	}
	/**
	 * 3.0根据  订单项 id 查询
	 * @return
	 */
	@Test
	public void findItemOrderByOrdId(){
		String ordId="506BAF29C9344061A80F746E6981B820";
		List<TItemorder> items= itemOrderService.findItemByOrderId(ordId);
		for (TItemorder item : items) {
			
			logger.info("itemorder   = "+item.toString());
		}
	}
	
	/**
	 * 4.0根据  订单项 id 修改
	 * @return
	 */
	@Test
	public void updateItemOrderByIteId(){
		String ordId="449E99B13FF745B6B42353FE216AABB3";
		TItemorderDto itemDto=new TItemorderDto();
		itemDto.setPkIteId(ordId);
		itemDto.setIteQuantity(6);
		itemDto.setIteSubtotal("120");
		int i= itemOrderService.modifyItem(itemDto) ;
		 logger.info("i = " +i  );
	}
	
	
	
	
	
	
	
	//setter
	public ItemOrderDao getItemOrderDao() {
		return itemOrderDao;
	}
	public void setItemOrderDao(ItemOrderDao itemOrderDao) {
		this.itemOrderDao = itemOrderDao;
	}

	public ItemOrderService getItemOrderService() {
		return itemOrderService;
	}

	public void setItemOrderService(ItemOrderService itemOrderService) {
		this.itemOrderService = itemOrderService;
	}
	
	
	
}

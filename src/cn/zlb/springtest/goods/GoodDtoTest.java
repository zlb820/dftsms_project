package cn.zlb.springtest.goods;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.google.gson.Gson;

import cn.zlb.dto.android.BaseDtoAnd;
import cn.zlb.dto.android.good.GoodDtoAndDetail;
import cn.zlb.dto.android.good.GoodsNode;

 

public class GoodDtoTest {
	private  static BaseDtoAnd<GoodsNode<GoodDtoAndDetail>> baseDto=new  BaseDtoAnd<GoodsNode<GoodDtoAndDetail>>();
	
	@Test
	public static  <E> BaseDtoAnd<GoodsNode<GoodDtoAndDetail>> baseDtoAnd(){
		//商品 01
		GoodDtoAndDetail detail1=new GoodDtoAndDetail();
		detail1.setName("辣条1");
		detail1.setPrice("80");
		detail1.setSellCount("100");
		
		GoodDtoAndDetail detail2=new GoodDtoAndDetail();
		detail2.setName("辣条2");
		detail2.setPrice("80");
		detail2.setSellCount("100");
		
		GoodDtoAndDetail detail3=new GoodDtoAndDetail();
		detail3.setName("辣条3");
		detail3.setPrice("80");
		detail3.setSellCount("100");
		//商品 02
		GoodDtoAndDetail detail11=new GoodDtoAndDetail();
		detail1.setName("方便面1");
		detail1.setPrice("80");
		detail1.setSellCount("100");
		
		GoodDtoAndDetail detail22=new GoodDtoAndDetail();
		detail2.setName("方便面2");
		detail2.setPrice("80");
		detail2.setSellCount("100");
		
		GoodDtoAndDetail detail33=new GoodDtoAndDetail();
		detail3.setName("方便面3");
		detail3.setPrice("80");
		detail3.setSellCount("100");
		
		//  商品集合01
		List<GoodDtoAndDetail> list1=new ArrayList<GoodDtoAndDetail>();
		list1.add(detail1);
		list1.add(detail2);
		list1.add(detail3);
	//  商品集合02
		List<GoodDtoAndDetail> list2=new ArrayList<GoodDtoAndDetail>();
		list2.add(detail11);
		list2.add(detail22);
		list2.add(detail33);
		//pageitem 节点1
		GoodsNode<GoodDtoAndDetail> node=new GoodsNode<GoodDtoAndDetail>();
		node.setSortName("辣条");
		node.setList(list1);
		
		//pageitem节点02
		GoodsNode<GoodDtoAndDetail> node2=new GoodsNode<GoodDtoAndDetail>();
		node2.setSortName("方便面");
		node2.setList(list2);
		
		//pageitem
		List<GoodsNode<GoodDtoAndDetail>> nodeList=(List<GoodsNode<GoodDtoAndDetail>>) new ArrayList<E>();
		nodeList.add(node);
		nodeList.add(node2);
		
		baseDto.setLogin(false);
		baseDto.setPageItems(nodeList);
		
		Gson gson=new Gson();
		String gsonStr=gson.toJson(baseDto);
		System.out.println(gsonStr);
		
		return baseDto;
		
	}
	
}

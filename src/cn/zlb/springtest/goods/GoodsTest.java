package cn.zlb.springtest.goods;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import cn.zlb.dto.GoodsDto;
import cn.zlb.dto.android.BaseDtoAnd;
import cn.zlb.dto.android.good.GoodAnalyze;
import cn.zlb.dto.android.good.GoodDtoAndDetail;
import cn.zlb.dto.android.good.GoodsNode;
import cn.zlb.entity.TGoods;
import cn.zlb.entity.TPictures;
import cn.zlb.entity.TStore;
import cn.zlb.entity.TStoreCategory;
import cn.zlb.service.CategoryService;
import cn.zlb.service.GoodsService;
import cn.zlb.service.StoreService;
import cn.zlb.springtest.BaseTest;
import cn.zlb.tools.CommonUtils;
import cn.zlb.tools.JsonUtils;
import cn.zlb.tools.Pager;
import uk.ltd.getahead.dwr.util.Logger;

public class GoodsTest extends BaseTest {
	private BaseDtoAnd<GoodsNode<GoodDtoAndDetail>> storeInfo=new BaseDtoAnd<GoodsNode<GoodDtoAndDetail>>();
	@Resource
	private GoodsService goodsService;
	Logger logger =Logger.getLogger(GoodsTest.class);
	/**
	 * 1.0 添加商品
	 */
	@Test
	@Rollback(true)
	public void addGoods(){
		TStore store =new TStore();
		store.setFkStoId("120BB352E8FD410CB10E78771E7A6A56");
		TStoreCategory category=new TStoreCategory();
		category.setPkCatId("F31CD2011D20458BA8C01A3C4B94A99A");
		TPictures pic=new TPictures();
		pic.setPkPicId("01EA56310FD14BE38EE6AC5407D78860");
		
		TGoods goods=new TGoods();
		goods.setPkGooId(CommonUtils.uuid());
		goods.setGooName("馒头");
		goods.setGooCurrprice("2");
		goods.setGooPrice("3");
		
		goods.setTStore(store);
		goods.setTStoreCategory(category);
		goods.setTPicturesByFkPictureSmall(pic);
		Serializable id=goodsService.add(goods);
		logger.info("add goods "+id);
		
	}
	/**
	 * 2.0查询 goods
	 */
	@Test
	public void findGoods(){
		TStore store =new TStore();
		store.setFkStoId("2D701DCB895A4CD3A41B262E34B3A690");
		 
		
		GoodsDto gooDto=new GoodsDto();
		//根据 店铺查找
		//gooDto.setTStore(store);
		//根据 目录查找
		gooDto.setPkCatId("E66E6C53D2844E088B90CD72A3D4D9AF");
		
		Pager<TGoods> pager=goodsService.findGoods(gooDto, new Pager<TGoods>());
		
		for (TGoods goods : pager.getResultList()) {
			logger.info("goods   =  " +goods.getGooName() + " and  " +goods.getGooCurrprice());
		}
	}
	
	/**
	 * 3.0 模糊查询
	 */
	@Test
	public void searchGoods(){
	GoodsDto gooDto=new GoodsDto();
	//按照 名称 模糊查询
	//gooDto.setGooName("瓜");
	//按照价格区间查询
	gooDto.setGooCurrprice("28");
	gooDto.setMaxPrice("35");
	Pager<TGoods> pager =goodsService.searchGoods(gooDto, new Pager<TGoods>());
	
	for (TGoods goods : pager.getResultList()) {
		logger.info("goods   =  " +goods.getGooName() + " and  " +goods.getGooCurrprice());
	}
	}
	
	/**
	 * 4.0 模糊查询
	 */
	@Test
	public void searchGoodsByName(){
	GoodsDto gooDto=new GoodsDto();
	//按照 名称 模糊查询
	gooDto.setGooName("瓜");
	
	Pager<TGoods> pager =goodsService.searchGoodsByName(gooDto.getGooName(), new Pager<TGoods>());
	
	for (TGoods goods : pager.getResultList()) {
		logger.info("goods   =  " +goods.getGooName() + " and  " +goods.getGooCurrprice());
	}
	}
	/**
	 * 5.0 id
	 * 查询
	 */
	@Test
	public void searchGoodsById(){
 
	//按照 名称 模糊查询
	 
	
	TGoods goods =goodsService.findGoodsById("2F5422FB88B04EDD94C39E4C51073E89");
 
		logger.info("goods   =  " +goods.getGooName() + " and  " +goods.getGooCurrprice());
	}
	
	
	/**
	 * 6.0 android 店铺测试
	 * 
	 */
	//service
	@Resource
	private CategoryService categoryService;
	@Resource
	private StoreService storeService;
	@Resource
	private GoodsService goodService;
	
	
	@Test
	public void stoDetail(){
		//1-0 获取参数 gson 转化
		 //GoodAnalyze goodAnalyze=JsonUtils.getInstance().fromJson(stoStr, GoodAnalyze.class);
		//1-1  找出店铺
		 
			//TStore store=storeService.findStore(stoDto);
			
			//1-2   找出店铺优惠信息
			/*CheapDto cheDto=new CheapDto();
			cheDto.setFkStoId(store.getFkStoId());
			List<TCheap> cheList=cheapService.findCheap(cheDto);*/
			
			//1-3 找出店铺目录信息
			List<TStoreCategory> categoryWithPids=categoryService.findCategoryWithPid("120BB352E8FD410CB10E78771E7A6A56");
			List<GoodsNode<GoodDtoAndDetail>> nodeList = storeInfoManage(categoryWithPids);
			
			storeInfo.setPageItems(nodeList);
			System.out.println("查询结果 = " +JsonUtils.getInstance().toJson(storeInfo));
		 
	}
	private List<GoodsNode<GoodDtoAndDetail>> storeInfoManage(List<TStoreCategory> categoryWithPids) {
		GoodsDto gooDto=new GoodsDto();
		GoodDtoAndDetail detail=null;
		List<GoodDtoAndDetail> detailList=null;
		GoodsNode<GoodDtoAndDetail> node=null;
		List<GoodsNode<GoodDtoAndDetail>> nodeList=new ArrayList<GoodsNode<GoodDtoAndDetail>>();
		for (TStoreCategory cate : categoryWithPids) {
			//1-3-1 根据二级目录找出 二级目录下的所有商品 
			gooDto.setPkCatId(cate.getPkCatId());
			List<TGoods> goods=goodService.findGoodsByStoreId(gooDto);
			
			detailList= new ArrayList<GoodDtoAndDetail>();
			for (TGoods goo : goods) {
				detail=new GoodDtoAndDetail();
				detail.setId(goo.getPkGooId());
				detail.setImgUrl(goo.getTPicturesByFkPictureSmall().getPicUrl());
				detail.setName(goo.getGooName());
				detail.setPrice(goo.getGooPrice());
				detail.setSellCount(goo.getGooSales());
				
				detailList.add(detail);
				
			}
			node=new GoodsNode<GoodDtoAndDetail>();
			node.setSortName(cate.getCatName());
			node.setList(detailList);
			
			nodeList.add(node);
			 
			
		}
		return nodeList;
	}
	
	
	//setter
	public BaseDtoAnd<GoodsNode<GoodDtoAndDetail>> getStoreInfo() {
		return storeInfo;
	}
	public void setStoreInfo(BaseDtoAnd<GoodsNode<GoodDtoAndDetail>> storeInfo) {
		this.storeInfo = storeInfo;
	}
	public GoodsService getGoodsService() {
		return goodsService;
	}
	public void setGoodsService(GoodsService goodsService) {
		this.goodsService = goodsService;
	}
	public Logger getLogger() {
		return logger;
	}
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	public CategoryService getCategoryService() {
		return categoryService;
	}
	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	public StoreService getStoreService() {
		return storeService;
	}
	public void setStoreService(StoreService storeService) {
		this.storeService = storeService;
	}
	public GoodsService getGoodService() {
		return goodService;
	}
	public void setGoodService(GoodsService goodService) {
		this.goodService = goodService;
	}
	
	
}












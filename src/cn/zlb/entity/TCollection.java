package cn.zlb.entity;

public class TCollection {
	private String pkColId;
	private TCustomer TCustomer;
	private TStore TStore;
	 
	public String getPkColId() {
		return pkColId;
	}
	public void setPkColId(String pkColId) {
		this.pkColId = pkColId;
	}
	public TCustomer getTCustomer() {
		return TCustomer;
	}
	public void setTCustomer(TCustomer tCustomer) {
		TCustomer = tCustomer;
	}
	public TStore getTStore() {
		return TStore;
	}
	public void setTStore(TStore tStore) {
		TStore = tStore;
	}
	
	
}
